<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 23.04.18
 * Time: 15:03
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/**
 * @var \common\models\forms\ForgotPasswordForm $model
 */

$this->title = 'Сброс пароля';
?>

<div class="col-md-8 col-md-offset-2">
    <section id="description" class="card">
        <div class="card-header">
            <h4 class="card-title"><?= $this->title ?></h4>
        </div>
        <div class="card-body collapse in">
            <div class="card-block">
                <div class="card-text">
                    <?php $form = ActiveForm::begin(['enableAjaxValidation' => true]) ?>
                    <?= $form->field($model, 'email')->textInput() ?>
                    <?= Html::submitButton('Запросить сброс пароля', ['class' => 'btn btn-success']) ?>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </section>
</div>